# Generated by Django 3.1.7 on 2021-03-31 10:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AdminConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('cid', models.FloatField(default=0)),
                ('website_name', models.CharField(blank=True, max_length=100)),
                ('website_status', models.BooleanField(default=True)),
                ('allow_user_signup', models.BooleanField(default=False)),
                ('recaptha_enabled', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='ContactUS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(choices=[('GENERAL', 'Business'), ('SALES', 'sales'), ('BILLING', 'Billing'), ('SUPPORT', 'Support')], default='GENERAL', max_length=50)),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('email', models.EmailField(max_length=254)),
                ('mobile', models.CharField(blank=True, max_length=15, null=True)),
                ('message', models.TextField(blank=True, max_length=500, null=True)),
                ('status', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
