# Adminapp/ Package_management.py
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib import messages, auth
from django.shortcuts import render, redirect

from adminapp.views import session_required
from packages.models import Package, Features
from django.contrib.auth import get_user_model

User = get_user_model()


# List All Pages
@method_decorator(session_required, name='dispatch')
class AllPackages(generic.View):

    def get(self, request):
        packages = Package.objects.all()
        context = {'packages': packages}
        return render(request, 'adminapp/packages.html', context)


@method_decorator(session_required, name='dispatch')
class CreatePackage(generic.View):
    def get(self, request):
        features = Features.objects.all()
        context = {'features': features}
        return render(request, 'adminapp/create_package.html', context)

    def post(self, request):
        name = request.POST.get('name')
        validity = request.POST.get('validity')
        price = request.POST.get('price')
        commission = request.POST.get('commission')
        feature = request.POST.get('feature')
        features = request.POST.get('features')
        is_free = request.POST.get('isfree')
        if is_free is not None:
            is_free = True
        else:
            is_free = False

        new_pack = Package.objects.create(package_name=name, feature=feature, validity=validity,
                                          affiliate_commission=commission, price=price, is_free=is_free)
        new_pack.features.add(*features)

        messages.add_message(request, messages.SUCCESS, 'Package created Successfully')
        return redirect('/adminapp/create-package/')


@method_decorator(session_required, name='dispatch')
class UpdatetePackage(generic.View):
    def get(self, request, id):
        package = Package.objects.get(id=id)
        features = Features.objects.all()
        context = {'package': package, 'features': features}
        return render(request, 'adminapp/update_package.html', context)

    def post(self, request, id):
        name = request.POST.get('name')
        validity = request.POST.get('validity')
        price = request.POST.get('price')
        commission = request.POST.get('commission')
        feature = request.POST.get('feature')
        features = request.POST.get('features')
        is_free = request.POST.get('isfree')
        current_pack = Package.objects.get(id=id)
        current_pack.name = name
        current_pack.validity = validity
        current_pack.feature = feature
        current_pack.price = price
        current_pack.affiliate_commission = commission
        current_pack.save()
        if features is not None:
            current_pack.features.add(*features)  # Set features to the Newly created packages

        messages.add_message(request, messages.SUCCESS, 'Package Updated Successfully')
        return redirect('/adminapp/all-packages/')

    def delete(self, request, id):
        package = Package.objects.get(id=id)
        package.delete()
        messages.add_message(request, messages.WARNING, 'Package Deleted Successfully')
        return redirect('/adminapp/all-packages/')
