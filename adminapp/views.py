# Admin/views.py
from datetime import date
from django.shortcuts import render, redirect
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Sum
from django.contrib import messages, auth
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth import logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from django.utils.decorators import method_decorator
from django.views import generic
from django.http import HttpResponse
from django.contrib.auth import get_user_model
from django.conf import settings
from .models import AdminConfig
from payments.models import Order

User = get_user_model()
today = date.today()  # Get Today date


# Create your views here

# Login Required Decorator
def session_required(function):
    def _function(request, *args, **kwargs):
        if request.session.get('admin') is None:
            return redirect('adminapp:login')
        return function(request, *args, **kwargs)

    return _function


@session_required
def index(request):
    total_users = User.objects.filter(user_scope="END_USER").count()
    joined_today = User.objects.filter(date_joined__contains=today).count()
    total_revenue = Order.objects.filter(isPaid=True).aggregate(Sum('order_amount'))['order_amount__sum']
    today_revenue = Order.objects.filter(isPaid=True, order_date__contains=today).aggregate(Sum('order_amount'))['order_amount__sum']
    today_revenue = 0 if today_revenue is None else today_revenue
    context = {'total_users': total_users,'joined_today': joined_today, 'total_revenue':total_revenue,'today_revenue':today_revenue}
    print("testing Data :", joined_today)
    return render(request, 'adminapp/index.html', context)


@session_required
def home(request):
    return render(request, 'adminapp/index.html')


@method_decorator(session_required, name='dispatch')
class GeneralSettings(generic.View):

    def get(self, request):
        try:
            setting = AdminConfig.objects.get(id=1)
        except:
            setting = None
        context = {'setting': setting}
        print("Settings :", setting)
        return render(request, 'adminapp/general_settings.html', context)

    def post(self, request):
        cid = request.POST.get('cid')
        site_name = request.POST.get('site_name')
        website_status = request.POST.get('website_status')
        allow_user_signup = request.POST.get('allow_user_signup')
        recaptha_enabled = request.POST.get('recaptha_enabled')

        if website_status is None:
            website_status = False
        else:
            website_status = True

        if allow_user_signup is None:
            allow_user_signup = False
        else:
            allow_user_signup = True

        if recaptha_enabled is None:
            recaptha_enabled = False
        else:
            recaptha_enabled = True

        try:
            setting = AdminConfig.objects.get(id=1)
            setting.cid = cid
            setting.website_name = site_name
            setting.website_status = website_status
            setting.allow_user_signup = allow_user_signup
            setting.recaptha_enabled = recaptha_enabled
            setting.save()
        except:
            AdminConfig.objects.create(cid=cid, website_name=site_name, website_status=website_status,
                                       allow_user_signup=allow_user_signup, recaptha_enabled=recaptha_enabled)

        messages.add_message(request, messages.SUCCESS, 'Settings Updated Successfully')
        return redirect('adminapp:general-settings')


# login page
class getLogin(generic.View):
    def get(self, request):
        if request.user is not None:
            userid = request.user
            if str(userid) != "AnonymousUser":
                print("User :", userid)
                auth_user = User.objects.get(username=userid)
                if auth_user.user_scope == "ADMIN":
                    return redirect('adminapp:home')
                else:
                    return redirect('adminapp:index')
            else:
                return render(request, "adminapp/login.html", {})
        else:
            return render(request, "adminapp/login.html", {})

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        record = User.objects.filter(email=username).exists()
        if not record:
            messages.add_message(request, messages.WARNING, 'User DoesNot Exist')
            return redirect('adminapp:login')
        auth_user = authenticate(username=username, password=password)
        if auth_user is not None:
            # login(request,auth_user)
            if auth_user.user_scope == "ADMIN":
                request.session['admin'] = {
                    'id': auth_user.id,
                    'name': auth_user.username,
                    'mobile': auth_user.phone_number,
                    'status': auth_user.user_status,
                }
                return redirect('adminapp:index')

            else:
                messages.add_message(request, messages.WARNING, 'Invaild Session - Please Try Again')
                return redirect('adminapp:login')

            # return redirect('customer:Dashboard')

        else:
            messages.add_message(request, messages.WARNING, 'Username or password mismatch')
            return redirect('adminapp:login')


# Logout function
def logout(request):
    for key in list(request.session.keys()):
        del request.session[key]
    django_logout(request)
    return redirect('adminapp:login')
