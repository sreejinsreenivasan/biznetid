# Admin/Models.py
from django.db import models


# Create your models here.

class ContactUS(models.Model):
    SUBJECT_TYPES = [("GENERAL", "Business"), ("SALES", "sales"), ("BILLING", "Billing"), ("SUPPORT", "Support")]
    subject = models.CharField(max_length=50, choices=SUBJECT_TYPES, default="GENERAL")
    name = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(blank=False, null=False)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    message = models.TextField(max_length=500, blank=True, null=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class AdminConfig(models.Model):
    id = models.AutoField(primary_key=True)
    cid = models.FloatField(default=0)  # configuration ID
    website_name = models.CharField(max_length=100, blank=True)
    website_status = models.BooleanField(default=True)
    allow_user_signup = models.BooleanField(default=False)
    recaptha_enabled = models.BooleanField(default=False)

    def __str__(self):
        return str(self.cid)

# List Config Model will be updated later.
