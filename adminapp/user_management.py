# Adminapp/ User_management.py
from .admin_settings import group_required
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib import messages, auth
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from .views import session_required

User = get_user_model()


# List All Users
@method_decorator(session_required, name='dispatch')
class AllUsers(generic.View):
    # permission_required =
    def get(self, request):
        users = User.objects.filter(user_scope="END_USER")
        context = {'users': users}
        return render(request, 'adminapp/list_users.html', context)


# Approve a User
@session_required
def approveUser(request, id):
    if request.method == 'POST':
        user = User.objects.get(id=id)
        user.is_active = True
        user.save()
        messages.add_message(request, messages.SUCCESS, 'User Approved Successfully')
        return redirect('/adminapp/allusers/')
    else:
        messages.add_message(request, messages.ERROR, 'Method NOT Alowed')
        return redirect('/adminapp/allusers/')


# Disable a User
@session_required
def disableUser(request, id):
    if request.method == 'POST':
        user = User.objects.get(id=id)
        user.is_active = False
        user.save()
        messages.add_message(request, messages.ERROR, 'User Disabled Successfully')
        return redirect('/adminapp/allusers/')
    else:
        messages.add_message(request, messages.ERROR, 'Method NOT Alowed')
        return redirect('/adminapp/allusers/')


# List All Active Users
@session_required
def activeUsers(request):
    users = User.objects.filter(user_scope="END_USER", is_active=True)
    context = {'users': users}
    return render(request, 'adminapp/active_users.html', context)


# List All Pending Users
@session_required
def pendingUsers(request):
    users = User.objects.filter(user_scope="END_USER", is_active=False)
    context = {'users': users}
    return render(request, 'adminapp/pending_users.html', context)
