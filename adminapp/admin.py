# Adminapp / Admin.py
from django.contrib import admin
from .models import AdminConfig, ContactUS

# Register your models here.
admin.site.register(AdminConfig)
admin.site.register(ContactUS)