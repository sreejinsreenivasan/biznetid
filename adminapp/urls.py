# AdminApp/Urls.py

from django.urls import path
from .views import *
from .user_management import *
from .list_management import *
from .packages_managements import *
from .template_management import *
from .transactions import *
from .admin_settings import *

app_name = "adminapp"

urlpatterns = [
    path('', index, name="index"),
    path('login/', getLogin.as_view(), name='login'),
    path('logout/', logout, name='logout'),
    path('home/', home, name="home"),
    # User management
    path('allusers/', AllUsers.as_view(), name="allusers"),
    path('activeusers/', activeUsers, name="activeusers"),
    path('pendingusers/', pendingUsers, name="pendingusers"),
    path('approve-user/<int:id>', approveUser, name="approve-user"),
    path('disable-user/<int:id>', disableUser, name="disable-user"),
    # List Management
    path('all-list/', AllList.as_view(), name="all-list"),
    path('approve-list/<int:id>', approveList, name="approve-list"),
    path('disable-list/<int:id>', disableList, name="disable-list"),
    # Visiting card Management
    path('all-cards/', AllCard.as_view(), name="all-cards"),
    path('approve-card/<int:id>', approveCard, name="approve-card"),
    path('disable-card/<int:id>', disableCard, name="disable-card"),
    # Package management
    path('all-packages/', AllPackages.as_view(), name="all-packages"),
    path('create-package/', CreatePackage.as_view(), name="create-package"),
    path('update-package/<int:id>/', UpdatetePackage.as_view(), name="update-package"),
    # Themes Management
    path('all-themes/', AllThemes.as_view(), name="themes"),
    path('create-theme/', CreateThemes.as_view(), name="create-theme"),
    path('update-theme/<int:id>/', UpdateteThemes.as_view(), name="update-theme"),
    # Order and Transactions management
    path('orders/', orderDetails, name="orders"),
    path('wallet/', WalletPayoutReq.as_view(), name="wallet"),
    path('wallet/<int:id>', WalletPayoutReq.as_view(), name="wallet"),
    path('generate-reports/', GenerateReports.as_view(), name="generate-reports"),
    path('generate-sheet/', excelReports, name="generate-sheet"),
    path('update-payment/', UpdatePayment.as_view(), name="update-payment"),
    # general Site Settings
    path('general-settings/', GeneralSettings.as_view(), name="general-settings"),
    path('create-payment-methods/', CreatePaymentTMethods.as_view(), name="create-payment-types"),
    path('view-payment-methods/', ViewPaymentTMethods.as_view(), name="view-payment-types"),

]
