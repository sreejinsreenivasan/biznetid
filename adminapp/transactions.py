# AdminApp / Transactions.py
import xlwt
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib import messages, auth
from django.shortcuts import render, redirect
from adminapp.views import session_required
from payments.models import Order
from wallet.models import Transactions, WalletPayout
from django.http import HttpResponse
from django.contrib.auth import get_user_model

User = get_user_model()

@session_required
def orderDetails(request):
    orders = Order.objects.all()
    context = {'orders': orders}
    return render(request, 'adminapp/payments.html', context)


@method_decorator(session_required, name='dispatch')
class GenerateReports(generic.View):
    def get(self, request):
        report_data = WalletPayout.objects.filter(payment_status=True, is_paid=False)
        context = {'report_data': report_data}
        print("Report :", report_data)
        return render(request, 'adminapp/generate_reports.html', context)


@method_decorator(session_required, name='dispatch')
class WalletPayoutReq(generic.View):

    def get(self, request):
        payout_req = WalletPayout.objects.all()
        context = {'payouts': payout_req}
        return render(request, 'adminapp/view_payout_req.html', context)

    def post(self, request, id):
        payout_req = WalletPayout.objects.get(id=id)
        payout_req.payment_status = True
        payout_req.save()
        messages.add_message(request, messages.SUCCESS, 'Payout Request Approved Successfully')
        return redirect('/adminapp/wallet/')


@method_decorator(session_required, name='dispatch')
class UpdatePayment(generic.View):
    def get(self, request):
        return render(request, 'adminapp/update_payments.html')

    def post(self, request):
        txn_id = request.POST.get('txn_id')
        payment_id = request.POST.get('payment_id')
        try:
            txn = WalletPayout.objects.get(txn_id=txn_id)
            if txn.is_paid:
                messages.add_message(request, messages.WARNING, 'Payment Id already Added for this Transaction')
                return redirect('adminapp:update-payment')
            else:
                txn.payment_reference = txn_id
                txn.is_paid = True
                txn.save()
                messages.add_message(request, messages.SUCCESS, 'Payment ID added Successfully')
                return redirect('adminapp:update-payment')
        except:
            messages.add_message(request, messages.ERROR, 'Enterd Transaction id Doesnot Exist')
            return redirect('adminapp:update-payment')


# generate Excell Reports
@session_required
def excelReports(request):
    report = WalletPayout.objects.filter(payment_status=True, is_paid=False).values_list('id', 'txn_id',
                                                                                         'payout_amount',
                                                                                         'payment_mode',
                                                                                         'payout_bankacc',
                                                                                         'payout_bankifsc',
                                                                                         'payment_reference',
                                                                                         'created_at')
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename= BiznetID_reports.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Transactions')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.bold = True
    columns = ['ID', 'Transaction ID', 'Amount', 'Mode', 'Account / UPI', 'IFSC', 'Provider', 'Date']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()

    for row in report:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, str(row[col_num]), font_style)
    wb.save(response)
    return response
