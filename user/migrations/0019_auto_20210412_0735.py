# Generated by Django 3.1.7 on 2021-04-12 07:35

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0018_auto_20210409_0916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='uuid',
            field=models.UUIDField(default=uuid.UUID('684868f5-61ca-4157-a89f-829fe8394209'), editable=False),
        ),
    ]
