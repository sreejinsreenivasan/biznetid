# Generated by Django 3.1.7 on 2021-03-30 11:28

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0008_auto_20210330_0546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='uuid',
            field=models.UUIDField(default=uuid.UUID('8a35a8bf-e9f5-4905-bb70-43539846d067'), editable=False),
        ),
    ]
