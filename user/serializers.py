# User/serializer.py
from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()


# List User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name','phone_number', 'email', 'username', 'referred_by']
        # fields = '__all__'


class DetailedUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name','phone_number', 'email', 'username', 'created_at',
                                                                                'is_affiliate', 'package']
        depth = 1