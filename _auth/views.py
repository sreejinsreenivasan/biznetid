# Auth / Views.py
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth import login, get_user_model
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status, generics, mixins, parsers, renderers
from rest_framework import generics, permissions
from rest_framework.response import Response
from .serializers import UserSerializer, RegisterSerializer, UserLoginSerializer, ChangePasswordSerializer
from knox.models import AuthToken  # For Knox token

User = get_user_model()


# Create your views here.
def index(request):
    return HttpResponse("Welcome to App Home")


# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        post_data = request.data
        try:
            referred_by = post_data['referred_by']
        except:
            referred_by = None
        user.referred_by = referred_by
        user.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


class RegisterByRefferal(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    permission_classes = (permissions.AllowAny,)

    def get(self,request, refid):
        response = {
            'referred_by': refid,
        }
        status_code = status.HTTP_206_PARTIAL_CONTENT

        return Response(response, status=status_code)


# Django REST Api with Authentication
class UserLoginView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data['email']
        user = User.objects.get(email=email)
        print("User :", user)
        response = {
            'success': 'True',
            'user': {'id': user.id, 'username': user.username, 'email': user.email},
            'token': serializer.data['token'],
        }
        status_code = status.HTTP_200_OK

        return Response(response, status=status_code)


#   An endpoint for changing password.
class ChangePasswordView(generics.UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
