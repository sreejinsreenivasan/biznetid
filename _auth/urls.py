# Auth / Urls.py
from django.urls import path, include
from rest_framework_simplejwt.views import TokenRefreshView
# from knox import views as knox_views
from .views import *
# from .auth import RegisterAPI, LoginAPI

app_name = "_auth"

urlpatterns = [
    path('', index, name="home"),
    path('register', RegisterAPI.as_view(), name='register'),
    path('register/<str:refid>', RegisterByRefferal.as_view(), name='regbyref'),
    path('login', UserLoginView.as_view(), name='login'),
    # path('logout', knox_views.LogoutView.as_view(), name='logout'),
    # path('logoutall', knox_views.LogoutAllView.as_view(), name='logoutall'),
    path('change-password', ChangePasswordView.as_view(), name='change-password'),

]