from rest_framework import permissions
from listing.models import List, Services
from cms.models import ServicesOfferd
from cms.models import Page

class IsOwnerOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        list_id = view.kwargs
        print("List ID :", list_id)
        try:
            id = list_id['id']
            request_user = request.user.username
            listing = List.objects.get(id=id)
            list_user = listing.user.username

        except:
            return False

        if list_user == request_user:
            return True

        return False


class IsWorking(permissions.BasePermission):

    def has_permission(self, request, view):
        user_id = view.kwargs
        print("Logged User :", user_id)
        return True


class IsPageOwner(permissions.BasePermission):

    def has_permission(self, request, view):
        obj = ServicesOfferd.objects.get(id=view.kwargs.get('pk'))
        has_perm = obj.page.user == request.user
        return has_perm
