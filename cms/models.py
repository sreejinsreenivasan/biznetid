# CMS / Models.py
from django.db import models
from django.contrib.auth import get_user_model
from django.db import IntegrityError
import uuid

User = get_user_model()


# Create your models here.

class Page(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, null=True, blank=True)
    name = models.CharField(max_length=90, null=True)
    site = models.OneToOneField('listing.List', related_name='page_listing', on_delete=models.CASCADE, blank=True,
                                null=True)
    short_desc = models.CharField(max_length=150, null=True)
    banner = models.ImageField(upload_to='banner_image', null=True, blank=True)
    logo = models.ImageField(upload_to='logo_image', null=True, blank=True)
    phone_number = models.CharField(max_length=15, null=True, blank=True)
    business_email = models.EmailField(null=True, blank=True)
    about = models.TextField(max_length=300, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class Address(models.Model):
    page = models.OneToOneField('Page', related_name="page_address", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    building_number = models.CharField(max_length=256)
    street = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    state = models.CharField(max_length=256)
    pincode = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


# class SocialLink(models.Model):
#     LINK_TYPE = [("OTH", "Other links"), ("FB", "Facebook"), ("TWT", "Twitter"), ("INS", "Instagram"),
#                  ("YTB", "Youtube"), ("LN", "LinkedIn")]
#     page = models.ForeignKey('Page', related_name="sociallink", on_delete=models.CASCADE, null=True)
#     type = models.CharField(choices=LINK_TYPE, max_length=100, default="OTH")
#     url = models.CharField(max_length=100, null=True)
#     is_active = models.BooleanField(default=False)
#
#     def __str__(self):
#         return str(self.type)


class PageSocialLink(models.Model):
    page = models.ForeignKey('Page', related_name="page_sociallink", on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True, blank=True)
    fb_url = models.CharField(max_length=200, null=True, blank=True)
    twitter_url = models.CharField(max_length=200, null=True, blank=True)
    instagram_url = models.CharField(max_length=200, null=True, blank=True)
    linkedin_url = models.CharField(max_length=200, null=True, blank=True)
    youtube_url = models.CharField(max_length=200, null=True, blank=True)
    whatsapp_num = models.CharField(max_length=20, null=True, blank=True)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class CustomLink(models.Model):
    page = models.OneToOneField('Page', related_name="page_customlink", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    url = models.URLField()
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class TagItems(models.Model):
    page = models.ForeignKey('Page', related_name="page_tagitems", on_delete=models.CASCADE)
    tag = models.CharField(max_length=50, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.pk is None:
            tags_count = self.page.page_tagitems.all().count()
            if tags_count >= 6:
                raise IntegrityError
            else:
                super(TagItems, self).save(*args, **kwargs)
        else:
            super(TagItems, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.page.name)


class ServicesOfferd(models.Model):
    page = models.ForeignKey('Page', related_name="page_services", on_delete=models.CASCADE)
    service = models.CharField(max_length=50, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.pk is None:
            service_count = self.page.page_services.all().count()
            if service_count >= 6:
                raise IntegrityError
            else:
                super(ServicesOfferd, self).save(*args, **kwargs)
        else:
            super(ServicesOfferd, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.page.name)


class OperationHours(models.Model):
    page = models.OneToOneField('Page', related_name="page_hours", on_delete=models.CASCADE)
    monday = models.CharField(max_length=100, blank=True)
    tuesday = models.CharField(max_length=100, blank=True)
    wednesday = models.CharField(max_length=100, blank=True)
    thursday = models.CharField(max_length=100, blank=True)
    friday = models.CharField(max_length=100, blank=True)
    saturday = models.CharField(max_length=100, blank=True)
    sunday = models.CharField(max_length=100, blank=True)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.page)


class GalleryConfig(models.Model):
    name = models.CharField(max_length=20, null=True)
    max_uploads = models.IntegerField(default=6)
    package = models.ManyToManyField('packages.Package')
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class Gallery(models.Model):
    page = models.ForeignKey('Page', related_name="page_gallery", on_delete=models.CASCADE)
    gallery_type = models.ForeignKey('GalleryConfig', related_name="gc_gallery", on_delete=models.CASCADE)
    image = models.ImageField(upload_to='Gallery_images')
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.page)


class Reviews(models.Model):
    page = models.ForeignKey('Page', related_name="page_reviews", on_delete=models.CASCADE)
    package = models.ManyToManyField('packages.Package')
    name = models.CharField(max_length=100)
    email = models.EmailField()
    content = models.TextField(max_length=500, blank=True)
    rating = models.FloatField()
    is_disabled = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class Analytics(models.Model):
    page = models.ForeignKey('Page', related_name="page_analytics", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    content = models.TextField(max_length=500, blank=True)
    rating = models.FloatField()
    is_disabled = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class VisitingCard(models.Model):
    page = models.ForeignKey('Page', related_name="page_cards", on_delete=models.CASCADE)
    card_url = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    designation = models.CharField(max_length=100, blank=True, null=True)
    whatsapp = models.CharField(max_length=15, null=True, blank=True)
    facebook = models.CharField(max_length=50, null=True, blank=True)
    instagram = models.CharField(max_length=50, null=True, blank=True)
    youtube = models.CharField(max_length=50, null=True, blank=True)
    linkedin = models.CharField(max_length=50, null=True, blank=True)
    official_email = models.EmailField(max_length=50, null=True, blank=True)
    status = models.BooleanField(default=True)
    created_by = models.ForeignKey('user.User', related_name='visitingcard_user', blank=True, on_delete=models.PROTECT,
                                   null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class ContactUS(models.Model):
    page = models.ForeignKey('Page', related_name="page_contact", on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(blank=False, null=False)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    message = models.TextField(max_length=500, blank=True, null=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class Location(models.Model):
    page = models.ForeignKey('Page', related_name="page_location", on_delete=models.CASCADE)
    place = models.CharField(max_length=50, null=True, blank=True)
    lat = models.FloatField(null=True)
    lon = models.FloatField(null=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.page)


class PaymentTypes(models.Model):
    name = models.CharField(max_length=50, null=True)
    thumpnail = models.ImageField(upload_to='payment_image', null=True, blank=True)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class PaymentMethod(models.Model):
    page = models.ForeignKey('Page', related_name="page_paymodes", on_delete=models.CASCADE)
    payment_mode = models.ManyToManyField('PaymentTypes', null=True, blank=True)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.page)


# ------------- Models need to be Re Planned before final implemntation ----------
''' 
class SeoKeywords(models.Model):
    page = models.ForeignKey('Page', related_name="page_keywords", on_delete=models.CASCADE)
    keyword = models.CharField(max_length=200, null=True, blank=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.page)


'''
