# Generated by Django 3.1.7 on 2021-04-12 07:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_auto_20210409_0916'),
    ]

    operations = [
        migrations.CreateModel(
            name='TagItems',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(blank=True, max_length=50, null=True)),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='page_tagitems', to='cms.page')),
            ],
        ),
    ]
