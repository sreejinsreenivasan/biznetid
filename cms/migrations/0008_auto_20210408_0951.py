# Generated by Django 3.1.7 on 2021-04-08 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0007_visitingcard_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pagesociallink',
            name='social_link',
        ),
        migrations.AddField(
            model_name='pagesociallink',
            name='fb_url',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='pagesociallink',
            name='instagram_url',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='pagesociallink',
            name='linkedin_url',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='pagesociallink',
            name='twitter_url',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='pagesociallink',
            name='whatsapp_num',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='pagesociallink',
            name='youtube_url',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.DeleteModel(
            name='SocialLink',
        ),
    ]
