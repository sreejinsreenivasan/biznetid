from django import forms
from cms.models import PaymentTypes


class PaymentTypesForm(forms.ModelForm):
    class Meta:
        model = PaymentTypes
        fields = ('name', 'thumpnail')
