#Themes/Models.py
from django.db import models
from django.contrib.auth import get_user_model
import uuid

User = get_user_model()


# Create your models here.

class Templates(models.Model):
    tid = models.IntegerField(unique=True, null=True)
    name = models.CharField(max_length=100)
    status = models.BooleanField(default=True)
    is_free = models.BooleanField(default=False)
    created_by = models.ForeignKey('user.User', related_name='templates_user', blank=True, on_delete=models.PROTECT,
                                   null=True)
    bg_image = models.ImageField(upload_to='background_image', null=True, blank=True)
    thumbnail = models.ImageField(upload_to='thumbnail_image', null=True, blank=True)
    package = models.ManyToManyField('packages.Package', null=True, related_name="package_themes")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


