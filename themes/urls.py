from django.urls import path
from .views import *

app_name = "packages"

urlpatterns = [
    path('', index, name="home"),
    path('themes', TemplateAPIView.as_view(), name="themes"),
    path('themes/<int:id>', TemplateDetails.as_view(), name="themes-details"),
    path('set-theme',  SetPaidTheemes.as_view(), name="set-theme"),
]