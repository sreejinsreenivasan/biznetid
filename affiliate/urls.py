from django.urls import path
from .views import *

app_name = "affiliate"

urlpatterns = [
    path('', index, name="home"),
    path('account', AffiliateHome.as_view(), name="account"),
    path('downlines', DownlineUsers.as_view(), name="downlines"),
]