# Affilates / Views.py
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
# from rbac.permissions import IsWorking
from .models import Affiliates, BankAccounts
from .serializer import UserSerializer,BankAccountSerializer
from django.contrib.auth import get_user_model
import uuid
# For Test API
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from wallet.models import Wallet, Transactions

User = get_user_model()


# Create your views here.

def index(request):
    return HttpResponse("Welcome to Affiliate APP")


# create or Retrieve Affiliate details
class AffiliateHome(APIView):
    permission_classes = [IsAuthenticated]  # IsWorking

    def get(self, request):
        user = request.user
        try:
            affiliate_status = user.affiliates.affiliate_status
            affiliate_id = user.affiliates.affiliate_id
            serializer = BankAccountSerializer(user=user)
            status_msg = "Welcome, Your Affiliate ID is:" + str(affiliate_id)
            return Response(status_msg, status=status.HTTP_200_OK)

        except:
            status_msg = "You dont Have an Affiliate Account, Please create one."
            return Response(status_msg, status=status.HTTP_204_NO_CONTENT)

    def post(self, request):
        user = request.user
        try:
            affiliate_object = Affiliates.objects.get(user=user)
            affiliate_status = affiliate_object.affiliate_status
        except:
            affiliate_status = None

        if user.user_scope == "END_USER" and affiliate_status is None:
            affiliate_id = str(user.username)
            bank_acc = request.data['bank_acc']
            bank_ifsc = request.data['bank_ifsc']
            bank_name = request.data['bank_name']
            upi_id = request.data['upi_id']
            upi_method = request.data['upi_method']
            affiliate = Affiliates.objects.create(user=user, affiliate_id=affiliate_id, affiliate_status=True)
            BankAccounts.objects.create(user=affiliate, bank_acc_num=bank_acc, bank_ifsc_code=bank_ifsc,
                                        bank_name=bank_name,
                                        upi_id=upi_id, upi_method=upi_method)
            user.is_affiliate = True
            user.save()
            status_message = "Affiliate URL Created Success fully, ID is:" + str(affiliate_id)
            return Response(status_message, status=status.HTTP_201_CREATED)

        else:
            status_message = "User is already an Affiliate or Wrong User type."
            return Response(status_message, status=status.HTTP_400_BAD_REQUEST)


# Display Affiliate Downsides
class DownlineUsers(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        downlines = []
        username = request.user.username
        all_downline = User.objects.filter(referred_by=username, package=True)
        list_data = [{'username': x.username, 'package': x.package.package_name} for x in all_downline ]
        print(list_data)
        status_message = downlines
        return Response(list_data, status=status.HTTP_201_CREATED)


# Add Affiliate Commission after payment.
def affiliateCommission(user, amount):
    try:
        sponsorid = user.referred_by
        sponsor = User.objects.get(username=sponsorid)
        commission_percentage = sponsor.package.affiliate_commission
        commission_amnt = int(amount) * (int(commission_percentage) / 100)
        commission_amount = round(commission_amnt, 2)
        print(" Sponsor :", sponsor, "Amount :", amount, "Commission Percentage :", commission_percentage,
              " Commission amnt:", commission_amount)
        # aff_commission = Wallet.objects.create(user=sponsor, is_active=True, primary_wallet=pw)
        wallet = sponsor.wallet
        aff_transaction = Transactions.objects.create(wallet=wallet, amount=commission_amount, type="CREDIT")
        sponsor.wallet.add_primary_wallet(commission_amount)
        print("Affiliate Withdrawal :", aff_transaction)
    except:
        print("User Doesn't have Up lines..")
    return True
