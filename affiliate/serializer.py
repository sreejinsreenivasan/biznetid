# Affiliates / Serializer
from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import BankAccounts

User = get_user_model()


# List User Serializer
class UserSerializer(serializers.Serializer):
    class Meta:
        model = User
        fields = ['username']


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccounts
        fields = ['id', 'user', 'bank_acc_num', 'bank_ifsc_code', 'bank_name', 'upi_id', 'upi_method']
