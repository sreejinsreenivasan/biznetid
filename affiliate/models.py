# Affiliates / Models.py
from django.db import models
from django.contrib.auth import get_user_model
import uuid

User = get_user_model()


# Create your models here
class Affiliates(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField('user.User', related_name="affiliates", on_delete=models.PROTECT, null=True)
    affiliate_id = models.CharField(max_length=50, null=True)
    uuid = models.UUIDField(null=True, blank=True, unique=True, editable=False)
    affiliate_status = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.uuid = uuid.uuid4()
        super(Affiliates, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.user)


class BankAccounts(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField('Affiliates', related_name="bankaccount", on_delete=models.PROTECT, null=True)
    bank_acc_num = models.CharField(max_length=20, null=True)
    bank_ifsc_code = models.CharField(max_length=20, null=True)
    bank_name = models.CharField(max_length=20, null=True)
    upi_id = models.CharField(max_length=30, null=True)
    upi_method = models.CharField(max_length=20, null=True)

    def __str__(self):
        return str(self.user)