# Generated by Django 3.1.7 on 2021-03-24 08:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('packages', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='package_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='package',
            name='features',
            field=models.ManyToManyField(blank=True, null=True, to='packages.Features'),
        ),
        migrations.AddField(
            model_name='package',
            name='validity',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='packages.packagevalidity'),
        ),
        migrations.AddField(
            model_name='features',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='feature_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
