# Generated by Django 3.1.7 on 2021-03-30 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0004_auto_20210325_0555'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='is_free',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
