# Packages/Admin.py
from django.contrib import admin
from .models import Package, Features, PackageValidity
# Register your models here.

admin.site.register(Package)
admin.site.register(Features)
admin.site.register(PackageValidity)