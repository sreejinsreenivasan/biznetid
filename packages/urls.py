from django.urls import path
from .views import *

app_name = "packages"

urlpatterns = [
    path('', index, name="home"),
    path('packages', PackageAPIView.as_view(), name="Packages"),
    path('package/<int:id>', PackageDetails.as_view(), name="package-details"),
    path('features', FeaturesAPIView.as_view(), name="features"),
    path('feature/<int:id>', FeatureDetails.as_view(), name="feature-details"),
]