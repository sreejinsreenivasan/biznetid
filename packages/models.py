# packages/Models.py
from django.db import models


# Create your models here.
# package Model with foreign key to  Features available model
class Package(models.Model):
    id = models.AutoField(primary_key=True)
    package_name = models.CharField(max_length=100, null=False)
    validity = models.IntegerField(default=365)
    features = models.ManyToManyField('Features', null=True, blank=True)
    feature = models.TextField(max_length=500, blank=True)
    exp_date = models.DateTimeField(null=True, blank=True)
    price = models.FloatField(default=0.00)
    status = models.BooleanField(default=False)
    is_free = models.BooleanField(default=False, null=True)
    affiliate_commission = models.IntegerField(null=True)
    created_by = models.ForeignKey('user.User', related_name='package_user', blank=True, on_delete=models.PROTECT,
                                   null=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.package_name)


class PackageValidity(models.Model):
    VALIDITY_DAYS = [(30, "30"), (90, "90"), (180, "180"), (365, "365")]
    id = models.AutoField(primary_key=True)
    validity = models.IntegerField(choices=VALIDITY_DAYS)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.validity)


# Available Features for each package.
class Features(models.Model):
    id = models.AutoField(primary_key=True)
    FEATURE_SCOPES = [("BASIC_FEATURE", "Basic"), ("PREMIUM_FEATURE", "Premium"), ("ADV_FEATURE", "Advanced")]

    feature_name = models.CharField(max_length=50, null=False)
    features = models.CharField(max_length=50, choices=FEATURE_SCOPES, default="BASIC_FEATURE")
    status = models.BooleanField(default=False)
    created_by = models.ForeignKey('user.User', related_name='feature_user', blank=True, on_delete=models.PROTECT,
                                   null=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.feature_name)