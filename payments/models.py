# Payments/Models.py
from django.db import models
from django.contrib.auth import get_user_model
import uuid

User = get_user_model()


# Create your models here.

class Transactions(models.Model):
    PAYMENT_TYPE = [("UPI", " UPI wallets"), ("NEFT", "Bank Transfer")]
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, null=True, blank=True)
    amount = models.FloatField(default=0.00)
    product = models.ForeignKey('packages.Package', on_delete=models.DO_NOTHING, null=True, blank=True)
    txn_id = models.CharField(max_length=50, null=True)
    order_payment_id = models.CharField(max_length=100, null=True)
    payment_type = models.CharField(choices=PAYMENT_TYPE, default="UPI", max_length=40)
    payment_status = models.BooleanField(default=False)
    payment_reference = models.CharField(max_length=100)
    signature = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.txn_id)


class Order(models.Model):
    PAYMENT_TYPE = [("UPI", " UPI wallets"), ("NEFT", "Bank Transfer")]
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, null=True, blank=True)
    order_product = models.CharField(max_length=100)
    order_amount = models.CharField(max_length=25)
    order_payment_id = models.CharField(max_length=100, null=True)
    txn_id = models.CharField(max_length=50, null=True)
    payment_type = models.CharField(choices=PAYMENT_TYPE, default="UPI", max_length=40)
    payment_reference = models.CharField(max_length=100, null=True)
    signature = models.CharField(max_length=100, null=True)
    isPaid = models.BooleanField(default=False)
    order_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.txn_id
