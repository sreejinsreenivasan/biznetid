# Payments/Serializer.py
from rest_framework import serializers
from .models import Transactions, Order
from django.contrib.auth import get_user_model

User = get_user_model()


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = '__all__'

    def validate(self, validated_data):
        userid = validated_data['user']
        product = validated_data['product']
        user = User.objects.get(username=userid)
        user.package = product
        user.save()
        print(" Payment data- User :", userid, "Product :", product)
        return validated_data


class OrderSerializer(serializers.ModelSerializer):
    order_date = serializers.DateTimeField(format="%d %B %Y %I:%M %p")

    class Meta:
        model = Order
        fields = '__all__'
        #depth = 1


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['user', 'order_product', 'order_amount', 'order_payment_id', 'txn_id', 'order_date']