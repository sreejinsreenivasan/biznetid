# Payments/views.py
import json
import razorpay
import uuid
from decouple import config
from django.http import HttpResponse
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from cms.views import paidPackage
from .models import Order, Transactions
from .serializer import PaymentSerializer, OrderSerializer, TransactionSerializer
from packages.models import Package
from affiliate.views import affiliateCommission
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    return HttpResponse("Welcome to App Home")


class Checkout(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = PaymentSerializer
    data = serializer_class.data
    queryset = Transactions.objects.all()


# Razorpay payment gateway
# @method_decorator(csrf_exempt, name='dispatch')
class StartPayment(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        # request.data is coming from frontend
        amount = request.data['amount']
        productid = request.data['product']
        user = request.user
        product = Package.objects.get(id=productid)
        txn_id = uuid.uuid4().hex
        # setup razorpay client
        client = razorpay.Client(auth=(config('RAZORPAY_PUBLIC_KEY'), config('RAZORPAY_SECRET_KEY')))

        # create razorpay order
        payment = client.order.create({"amount": int(amount) * 100, "currency": "INR", "payment_capture": "1"})

        # we are saving an order with isPaid=False
        order = Order.objects.create(user=user, txn_id=txn_id, order_product=product, order_amount=amount,
                                     order_payment_id=payment['id'])
        # txn = Transactions.objects.create(user=user, amount=amount, product=product, txn_id=txn_id,
        # order_payment_id=payment['id'])  # payment_status, payment_reference, signature)

        serializer = OrderSerializer(order)
        data = {
            "payment": payment,
            "order": serializer.data
        }
        return Response(data)


# @login_required
@api_view(['POST'])
def handle_payment_success(request):
    # request.data is coming from frontend
    res = json.loads(request.data["response"])

    # create Empty variables for payment response Objects
    ord_id = ""
    raz_pay_id = ""
    raz_signature = ""

    # res.keys() will give us list of keys in res
    for key in res.keys():
        if key == 'razorpay_order_id':
            ord_id = res[key]
        elif key == 'razorpay_payment_id':
            raz_pay_id = res[key]
        elif key == 'razorpay_signature':
            raz_signature = res[key]

    # get order by payment_id which we've created earlier with isPaid=False
    order = Order.objects.get(order_payment_id=ord_id)
    user = order.user

    data = {
        'razorpay_order_id': ord_id,
        'razorpay_payment_id': raz_pay_id,
        'razorpay_signature': raz_signature
    }

    client = razorpay.Client(auth=(config('RAZORPAY_PUBLIC_KEY'), config('RAZORPAY_SECRET_KEY')))

    # checking if the transaction is valid or not if it is "valid" then check will return None
    check = client.utility.verify_payment_signature(data)

    if check is not None:
        print("Redirect to error url or error page")
        return Response({'error': 'Something went wrong'})

    # if payment is successful that means check is None then we will turn isPaid=True
    order.isPaid = True
    order.payment_reference = data['razorpay_payment_id']
    order.signature = data['razorpay_signature']
    order.save()
    res_data = {
        'message': 'payment successfully received!'
    }
    try:
        package = order.order_product  # Fetch which package is chosen as Product while making payments
        amount = order.order_amount
        paidPackage(user, package)  # call Paid package Function for Set the package to USer model
        affiliateCommission(user, amount)  # Call Affiliate Commission function for Set Commission for Splines.
    # print("Payment Status", res_data)
    except:
        print("** Something Went wrong - Check Set package or Affiliate Commission models ..!!")
    return Response(res_data)


class Transactions(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        txn = Order.objects.filter(user=user)
        serializer = TransactionSerializer(txn, many=True)
        return Response(serializer.data)


