# Wallet / Urls.py
from django.urls import path
from .views import *

app_name = "wallet"

urlpatterns = [
    path('', index, name="home"),
    path('withdraw', WithdrawWallet.as_view(), name="withdraw"),
    path('approve/<int:id>', ApprovePayout.as_view(), name="approve"),

]