from django.db import models
from django.contrib.auth import get_user_model
import uuid

User = get_user_model()


# Create your models here.

class Wallet(models.Model):
    user = models.OneToOneField(User, related_name="wallet", on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False)
    primary_wallet = models.FloatField(default=0.00)
    secondary_wallet = models.FloatField(default=0.00)
    created_at = models.DateTimeField(auto_now=True)

    def add_primary_wallet(self, amount):
        if self.primary_wallet is not None:
            self.primary_wallet += amount
        else:
            self.primary_wallet = amount
        self.save()

    def subtract_primary_wallet(self, amount):
        if self.primary_wallet is not None:
            self.primary_wallet -= amount
        else:
            pass
        self.save()

    def __str__(self):
        return str(self.user)


class Transactions(models.Model):
    TRANSACTION_TYPES = [('CREDIT', 'Credit'), ('DEBIT', 'Debit')]

    wallet = models.ForeignKey(Wallet, related_name="transactions", on_delete=models.CASCADE)
    type = models.CharField(max_length=10, choices=TRANSACTION_TYPES)
    amount = models.FloatField()
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.wallet)


# Unwanted Model
class WalletPayout(models.Model):
    PAYMENT_TYPES = [("UPI", "UPI Wallet"), ("NEFT", "Bank Account Transfer")]
    user = models.ForeignKey('user.User', related_name="wallet_payout", on_delete=models.DO_NOTHING, null=True, blank=True)
    name = models.CharField(max_length=50)
    payout_amount = models.FloatField(default=0.00)
    txn_id = models.CharField(max_length=50)
    remarks = models.CharField(max_length=100)
    payment_mode = models.CharField(max_length=50, choices=PAYMENT_TYPES, default="UPI")
    payment_status = models.BooleanField(default=False)
    payout_bankacc = models.CharField(max_length=30, null=True)
    payout_bankifsc = models.CharField(max_length=30, null=True)
    payout_upid = models.CharField(max_length=30, null=True)
    payment_reference = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    is_paid = models.BooleanField(default=False)

    def __str__(self):
        return str(self.txn_id)
