from django.urls import include, path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.conf import settings
from django.conf.urls.static import static

url_config_v1 = [
    path("api/v1/", include("api.urls", namespace="api")),
    path("api/v1/auth/", include(("_auth.urls", 'auth'), namespace="auth")),
    path("api/v1/user/", include("user.urls", namespace="user")),
    # path("api/v1/notification/", include("notification.urls", namespace="notification")),
    path("api/v1/themes/", include("themes.urls", namespace="themes")),
    path("api/v1/packages/", include("packages.urls", namespace="packages")),
    path("api/v1/payments/", include("payments.urls", namespace="payments")),
    path("adminapp/", include("adminapp.urls", namespace="adminapp")),
    path("api/v1/rbac/", include("rbac.urls", namespace="rbac")),
    path("api/v1/cms/", include("cms.urls", namespace="cms")),
    path("api/v1/affiliate/", include("affiliate.urls", namespace="cms")),
    path("api/v1/listing/", include("listing.urls", namespace="listing")),
    path("api/v1/wallet/", include("wallet.urls", namespace="wallet")),
    path('api/v1/token', TokenObtainPairView.as_view()),
    path('api/v1/token/refresh', TokenRefreshView.as_view()),
    # path('api/v1/auth/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
