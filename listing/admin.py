# Listing/Admin.py
from django.contrib import admin
from .models import List, Services

# Register your models here.
admin.site.register(List)
admin.site.register(Services)
