# Listing/Permissions.py
from rest_framework import permissions
from .models import List, Services


class IsOwnerOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        list_id = view.kwargs
        id = list_id['id']
        try:
            request_user = request.user.username
            listing = List.objects.get(id=id)
            list_user = listing.user.username

        except:
            return False

        if list_user == request_user:
            return True

        return False
