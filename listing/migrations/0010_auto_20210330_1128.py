# Generated by Django 3.1.7 on 2021-03-30 11:28

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('listing', '0009_auto_20210330_0546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='list',
            name='uuid',
            field=models.UUIDField(default=uuid.UUID('4254bd58-5001-4a6e-93f5-747b3171aee0'), editable=False),
        ),
    ]
