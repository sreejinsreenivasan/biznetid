from django.urls import path
from .views import *

app_name = "rbac"

urlpatterns = [
    path('', index, name="home")
]
