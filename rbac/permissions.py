# Rbac/Permissions.py
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication
from cms.models import Page
from listing.models import List
from cms.models import VisitingCard


# Admin only permission class - Used for checking is the Auth Uer is a staff / Site Admin

class IsAdminOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        index_id = view.kwargs
        id = index_id['id']  # Fetch the ID post for Filter from Any Model, If required.(Currently Not Used)
        try:
            user_scope = request.user.user_scope
            # print("User Scope :", user_scope)
            if str(user_scope) in ["ADMIN", "SUPERADMIN"]:
                return True
            else:
                return False

        except:
            return False


class IsSuperAdminOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        index_id = view.kwargs
        id = index_id['id']  # Fetch the ID post for Filter from Any Model, If required.(Currently Not Used)
        try:
            user_scope = request.user.user_scope
        except:
            return False

        if str(user_scope) == "SUPERADMIN":
            return True
        else:
            return False


class IsVendor(permissions.BasePermission):

    def has_permission(self, request, view):
        index_id = view.kwargs
        id = index_id['id']  # Fetch the ID post for Filter from Any Model, If required.(Currently Not Used)
        try:
            user_scope = request.user.user_scope
            # print("User Scope :", user_scope)
            if str(user_scope) in ["ADMIN", "VENDOR"]:
                return True
            else:
                return False

        except:
            return False


# Allow Get method for All user and Allow Post for Only Authenticated User Roles.
class IsGetOrIsAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        # allow all GET requests
        if request.method == 'GET':
            return True

        elif request.user and request.user.is_authenticated:
            try:
                user_scope = request.user.user_scope
                if str(user_scope) in ["ADMIN", "END_USER"]:
                    return True
                else:
                    return False
            except:
                return False
        else:
            return False


# Allow Get method for All user and Allow Post for Only Authenticated User Roles.
class IsGetOrIsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        # allow all GET requests
        if request.method == 'GET':
            return True

        elif request.user and request.user.is_authenticated:
            try:
                user_scope = request.user.user_scope
                if str(user_scope) in ["ADMIN", "SUPERADMIN"]:
                    return True
                else:
                    return False
            except:
                return False
        else:
            return False


class IsWorking(permissions.BasePermission):

    def has_permission(self, request, view):
        user_id = view.kwargs
        print("Logged User :", user_id)
        return True


class IsOwnerOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        list_id = view.kwargs
        id = list_id['id']
        try:
            request_user = request.user.username
            listing = List.objects.get(id=id)
            list_user = listing.user.username

        except:
            return False

        if list_user == request_user:
            return True

        return False


class IsPageOwner(permissions.BasePermission):

    def has_permission(self, request, view):

        try:
            page_id = view.kwargs
            slug = page_id['slug']
        except:
            page_id = view.kwargs['id']
            slug = Page.objects.get(id=page_id)
        request_user = request.user
        try:
            list = List.objects.get(url_name=slug)
            page_owner = list.user

        except:
            print("Page Owner :", "Request User :", request_user)
            return False
        if page_owner == request_user:
            return True
        else:
            return False


# New codes for Page Owner permssion working
class IsPageOwner2(permissions.BasePermission):

    def has_permission(self, request, view):
        request_user = request.user
        page_id = view.kwargs['id']
        page_owner = Page.objects.get(id=page_id).user
        if page_owner == request_user:
            return True
        else:
            return False


class IsCardOwner(permissions.BasePermission):

    def has_permission(self, request, view):
        req_id = view.kwargs
        card_id = req_id['id']
        try:
            page = VisitingCard.objects.get(id=card_id).page
            page_owner = page.user
            request_user = request.user
            # print("Page owner - user:", page_owner, request_user)
        except:
            return False

        if page_owner == request_user:
            return True
        else:
            return False
